#!/bin/bash - 
#===============================================================================
#
#          FILE:  record-dvb.sh
# 
#         USAGE:  ./record-dvb.sh -a [DVB DEVICE] [-c CHANNEL] [-m MINUTES]
# 
#   DESCRIPTION:  Record a channel from DVB device for number of minutes
#                 Returns 0 on success, 1 on fail
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  media-tv/linuxtv-dvb-apps (azap, czap, szap, tzap)
#                 sys-apps/coreutils (cut, dd)
#                 sys-apps/coreutils (sleep)
#                 sys-process/procps (kill)
#
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR: Roger Zauner (rdz), rogerx [dot] oss [AT] gmail [dot] com
#       LICENCE: GPL-2 2023, Roger Zauner
#       COMPANY: 
#       CREATED: 2011.11.19 11:10:24 AM AKST
#      REVISION: 0.4  (2023.01.09)
#
#
# This script spawns two background processes; a 'zap' process to tune/set
# the channel, and a 'cat' process to create a stream file (ie. MPEG-2) from the
# specified DVB device.  The script then sleeps for the specified amount of time
# and awake to stop the two processes.  A trap is set at the beginning in case
# the user uses CTRL-C or a kill signal, in which case trap then kills the
# background processes before returning control to the terminal/user.
#
# If the trap fails and azap or cat persists, then:
# $ ps ax |grep "zap\|cap" |grep -v grep
#
# And manually kill the PID's.  However unlikely as this script was created for
# managing this specific task, aside from a timed recording mechanism.
#
# User defined variables are set at the top of the file.  Make sure you have
# the above package dependecies, although they are quite common.
#
# For more information:
# Zap - LinuxTVWiki
# http://linuxtv.org/wiki/index.php/Zap
#
# Original script location:
# http://rogerx.freeshell.org/files/bin/record-dvb.sh
#
# TODO: Use "http://www.shellcheck.net" and fix some not double quoted variable
#       definitions.  Becareful, some unquoted or further quoted variables is
#       required for proper variable expansion to occur in some cases!
#
# Change Log:
#
# 2016.12.24: This script can be replaced with something to the following:
# $ dvbv5-zap --adapter=1 \
#     --channels=/home/user/.mplayer/channels.conf \
#     "CHANNEL NAME" --input-format=zap --record \
#     --output=/home/user/tv/`date +%Y%m%d-%H%M`-chCHANNEL_NAME-29min.ts \
#     --timeout=1740
#
# Augment both of the duration times/timeouts (eg. 29min), channel names, &
# folder locations according to your system environment.
# NOTE: I have no idea if this will break on signal disruptions, or long files.
# ffmpeg -i /dev/dvb/adapter1/dvr0 /tmp/output.ts can also be used, but again
# with the above note, no idea how the command will react on weak signals or big
# files!  FFMPEG delays while recording, guess looking for stream header info.
#
# 2016.05.30: Replaced spaces within Channel Names with a dash.
# (ie. ${CHANNEL// /-}).  Cleaned-up some comments.
#
# 2015.04.01: Reverted to creating files using .ts suffix instead of .mpg, per
# standards.
#
# 2014.12.01: Found a bug where later versions of w_scan write VSB_8 instead of
# the usual VSB8.  Earlier versions of zap utilities cannot read this yet.  (Bug
# noted below the call for azap.)
#
# 2014.05.23: Thanks to Holger Beetz in Germany for catching missing quotes at
# line #197, around the FILE_NAME parameter for 'dd' and 'ZAP_COMMAND'.  The
# effect without quotes, users could not specify a DVB channel with spaces.
# Now, spaces within channel names (or filenames) should no longer be a problem.

#===============================================================================

# TODO: Convert variable names to lower case! All uppercase var names are
# typically reserved for internal BASH and exported system variables!

# TODO: CHECK azap & cat are installed and accessible

set -o nounset          # Treat unset variables as an error
shopt -s huponexit      # Allow killing this script with SIGUSR1
                        # (See trap below.)

DEBUG="${DEBUG:=0}"     # 0 = no debug output, 1 = show debug output
                        # Or enable debug with:
                        # DEBUG=1 record-dvb.sh -c [CHANNEL] -m [MIN]

#===============================================================================
# VARIABLES
#===============================================================================

# User Defined
# 
declare -i DVB_DEVICE_NUM="0"
# FIXME: DVB_DEVICE_NUM_MINOR probably is not correct for addressing devices
# under DVB_DEVICE_NUM.
declare -i DVB_DEVICE_NUM_MINOR="0"

# Use dbscan or w_scan to find channels for channels.conf
#
# Provide aliases for your channel frequences:
# KTVF DT:545028615:8VSB:65:68:4
# 11.1:545028615:8VSB:65:68:4
#
declare CHANNELS_CONF="${HOME}/.mplayer/channels.conf"

#declare SAVE_FOLDER="/stored/tv"
declare SAVE_FOLDER="/mnt/archive1/tv"

# FIXME: This variable is defined within record_stream function
# It uses variables unset (unbound) until later.
#declare FILE_NAME="ch${CHANNEL}-${MINUTES}min-`date +%Y%m%d-%H%M`.ts"


# Choose your zap command (ie. azap for atsc, czap, szap and tzap)
# TODO: The other zap commands (czap, szap, tzap) are untested and likely need
# other options coded into this script.
#declare ZAP_COMMAND="azap"
# dvbv5-zap"-a 1 -f 0 -d 0 --channels=/home/roger/.mplayer/channels.conf --input-format=zap -r Laff
# TODO: Requires "--input-format=zap" else error reading channels.conf!
declare ZAP_COMMAND="dvbv5-zap"

# This prevents recording collisions with any next subsequent scheduled
# recording.  Set this to the amount of time in seconds it takes for zap to
# release the DVB device.
declare -i EARLY_RELEASE=3

# Program Specific
declare CHANNEL="0"
declare -i MINUTES=0
declare -i SECONDS=0

declare -i PIDOF_AZAP=0
# TODO: change var name pidof_cat to pidof_dd
declare -i PIDOF_CAT=0      # NOTE: We now use dd isntead of cat.

declare OPTSTRING=":h:a:c:m:v"
declare USAGE="Usage: $0 [-a dvb device] [-c channel] [-m minutes] [-h help] [-v verbose]"


#===============================================================================
# FUNCTIONS
#===============================================================================
#TODO: rename _debug() with debug(), underscore prefix not needed for
#      functions.
_debug()        # ifdef style printf debug
{
    [ ${DEBUG} -ne 0 ] && "$@"
    #_debug echo "variable  my_var="${my_var}
}

# TODO: replace echo with safer printf, easy drop-in solution
#echo() { local IFS=' '; printf '%s\n' "$*"; }

get_options()   # Parse command line arg/option/param
{
    _debug echo "DEBUG: In function get_options"
    _debug echo "DEBUG: OPTSTRING = ${OPTSTRING}"

    while getopts "${OPTSTRING}" opt; do
        # FIXME: Following debug fails after integration of option -v (DEBUG=1)
        #_debug echo "DEBUG: Option Index=${OPTIND}, Option=${OPTARG}"
        # NOTE: When adding options, be sure to add them to $OPTSTRING above!
        case $opt in
            h) echo ${USAGE}
                exit 0
                ;;

            a) DVB_DEVICE_NUM=${OPTARG}
                ;;

            c) CHANNEL=${OPTARG}
                ;;

            m) MINUTES=${OPTARG}
                ;;

            v) DEBUG=1
                ;;

            \?) echo ${USAGE}
                exit 1
                ;;

            *) echo "Option -$OPTARG requires an argument."
                echo ${USAGE}
                ;;
        esac
    done

    _debug echo "DEBUG: CHANNEL=${CHANNEL}, MINUTES=${MINUTES}"
}

set_channel()   # Set the channel with azap command
{
    _debug echo "DEBUG: In function set_channel"

    # This locks the channel and doesn't exit.  Needs to be manually stopped.
    # FIXME: dvbv5-zap -a 1 -f 0 -d 0 --channels=/home/roger/.mplayer/channels.conf --input-format=zap -r Laff
    ${ZAP_COMMAND} -a ${DVB_DEVICE_NUM} -f ${DVB_DEVICE_NUM_MINOR} \
        -d ${DVB_DEVICE_NUM_MINOR} -c $HOME/.mplayer/channels.conf \
        -I zap -r "${CHANNEL}" >/dev/null 2>&1 &
    
    _debug echo "DEBUG: ${ZAP_COMMAND} -a ${DVB_DEVICE_NUM} \
        -f ${DVB_DEVICE_NUM_MINOR} -d ${DVB_DEVICE_NUM_MINOR} \
        -c $HOME/.mplayer/channels.conf -I zap -r "${CHANNEL}""
    # FIXME: Later versions of w_scan has a change of format for writing "VSB_8"
    # instead of the usual 8VSB.  This change occurred in either w_scan-20140118
    # or w_scan-2014072, and linuxtv-dvb-apps-1.1.1.20100223-r1 various zap
    # utilities cannot read this yet!

    let PIDOF_AZAP="${!}"
    
    if (( ${PIDOF_AZAP} == 0 )); then
        printf "Error starting azap.\n"
        exit 1
    fi
    
    _debug echo "DEBUG: PID of azap is ${PIDOF_AZAP}"

    # Once a channel is locked by azap, azap sends this to stdout:
    # "status 1f | signal 0127 | snr 0127 | ber 00000000 | unc 00000000 | FE_HAS_LOCK"
    #
    # Can I somehow also check for output on the background process for FE_HAS_LOCK??
    # AZAP_STATUS=`echo "status 1f | signal 0127 | snr 0127 | ber 00000000 | unc 00000000 | FE_HAS_LOCK" | cut -d \| -f 6`

    # TODO: Should check for tuner lock?
    # Should somehow grep FE_HAS_LOCK from azap info (see above AZAP_STATUS line)
    # I tried piping output from azap to 'while read', even tee or logfile and failed

    # TODO: Need to wait for tuner lock?
    # I don't think so, but cat may fail - leave this stub here incase problems happen in the future.
    #sleep 5s
}

# Record a stream using cat on specified DVB device
record_stream()
{
    # No longer implying streams are .mpg
    #declare FILE_NAME="${SAVE_FOLDER}/ch${CHANNEL}-${MINUTES}min-`date +%Y%m%d-%H%M`.mpg"
    # Replace spaces within Channel names with dashes using "// /-" using shell
    # parameters instead of using sed.
    declare FILE_NAME="${SAVE_FOLDER}/`date +%Y%m%d-%H%M`-ch${CHANNEL// /-}-${MINUTES}min.ts"

    _debug echo "DEBUG: In function record_stream"

    _debug echo "DEBUG: Using device:"
    _debug echo "DEBUG:  /dev/dvb/adapter${DVB_DEVICE_NUM}/dvr${DVB_DEVICE_NUM_MINOR}"
    _debug echo "DEBUG:  /dev/dvb/adapter${DVB_DEVICE_NUM}/frontend${DVB_DEVICE_NUM_MINOR}"
    _debug echo "DEBUG: Saving to filename: "${FILE_NAME}""
    
    #cat /dev/dvb/adapter${DVB_DEVICE_NUM}/dvr${DVB_DEVICE_NUM_MINOR} > ${FILE_NAME} &
    dd if=/dev/dvb/adapter${DVB_DEVICE_NUM}/dvr${DVB_DEVICE_NUM_MINOR} \
        of="${FILE_NAME}" conv=noerror &
    # FIXME: Getting a "Value too large for defined data type" when using 'cat'
    # try "dd if=/dev/dvb/adapter0/dvr0 conv=noerror"

    # NOTE: We now use dd instead of cat.
    let PIDOF_CAT="${!}"
    
    if (( ${PIDOF_CAT} == 0 )); then
        printf "Error starting cat command on device file.\n"
        kill ${PIDOF_AZAP}
        exit 1
    fi

    _debug echo "DEBUG: PID of cat/dd is ${PIDOF_CAT}"
    
    echo "Filename: ${FILE_NAME}"
}

sleep_while_recording()     # Sleep while recording
{
    _debug echo "DEBUG: Recording time ${MINUTES} minutes"
    _debug echo "DEBUG: Early release is set to ${EARLY_RELEASE} seconds"

    # Collision prevention with any next subsequent scheduled recordings.
    #
    # Subtract one minute to prevent collisions with subsequent records
    #let MINUTES=${MINUTES}-1
    
    # Only need five seconds or less, so convert minutes to seconds, subtract
    # 5 seconds (or less).  For the least value, time the about of time it takes
    # for azap to release the DVB device from FE_HAS_LOCK to exit.
    let "SECONDS=(${MINUTES}*60)-${EARLY_RELEASE}"

    _debug echo "DEBUG: To prevent recording collisions, adjusted " \
        "recording time to ${SECONDS} seconds, instead of $((${MINUTES}*60)) " \
        "seconds."

    sleep ${SECONDS}s
}


#===============================================================================
# MAIN
#===============================================================================
# TODO: CHECK azap & cat are installed and accessible

# IF CTRL-C or kill signal is sent, make sure to quit zap & cat before exiting!
trap "kill ${PIDOF_CAT} ${PIDOF_AZAP}" SIGINT SIGTERM EXIT
# Also, use "shopt -s huponexit" to use  "kill -signal USR1 $PID"


# "$@" is required to pass variables to function
get_options "$@"

set_channel

record_stream

sleep_while_recording


# Manually stop recording process.  Kill returns 0 on success, 1 on fail.

_debug echo "DEBUG: Done sleeping."
_debug echo "DEBUG: Stopping recording \"${PIDOF_CAT}, ${PIDOF_AZAP}\" PIDs"

kill ${PIDOF_CAT} ${PIDOF_AZAP}

