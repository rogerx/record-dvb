#===============================================================================
#
#          FILE:  record-dvb.sh
# 
#         USAGE:  ./record-dvb.sh -a [DVB DEVICE] [-c CHANNEL] [-m MINUTES]
# 
#   DESCRIPTION:  Record a channel from DVB device for number of minutes
#                 Returns 0 on success, 1 on fail
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  media-tv/linuxtv-dvb-apps (azap, czap, szap, tzap)
#                 sys-apps/coreutils (cut, dd)
#                 sys-apps/coreutils (sleep)
#                 sys-process/procps (kill)
#
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR: Roger Zauner (rdz), rogerx [dot] oss [AT] gmail [dot] com
#       LICENCE: GPL-2 2023, Roger Zauner
#       COMPANY: 
#       CREATED: 2011.11.19 11:10:24 AM AKST
#      REVISION: 0.4  (2023.01.09)
#
#
# This script spawns two background processes; a 'zap' process to tune/set
# the channel, and a 'cat' process to create a stream file (ie. MPEG-2) from the
# specified DVB device.  The script then sleeps for the specified amount of time
# and awake to stop the two processes.  A trap is set at the beginning in case
# the user uses CTRL-C or a kill signal, in which case trap then kills the
# background processes before returning control to the terminal/user.
#
# If the trap fails and azap or cat persists, then:
# $ ps ax |grep "zap\|cap" |grep -v grep
#
# And manually kill the PID's.  However unlikely as this script was created for
# managing this specific task, aside from a timed recording mechanism.
#
# User defined variables are set at the top of the file.  Make sure you have
# the above package dependecies, although they are quite common.
#
# For more information:
# Zap - LinuxTVWiki
# http://linuxtv.org/wiki/index.php/Zap
#
# Original script location:
# http://rogerx.freeshell.org/files/bin/record-dvb.sh
#
# TODO: Use "http://www.shellcheck.net" and fix some not double quoted variable
#       definitions.  Becareful, some unquoted or further quoted variables is
#       required for proper variable expansion to occur in some cases!
#
# Change Log:
#
# 2016.12.24: This script can be replaced with something to the following:
# $ dvbv5-zap --adapter=1 \
#     --channels=/home/user/.mplayer/channels.conf \
#     "CHANNEL NAME" --input-format=zap --record \
#     --output=/home/user/tv/`date +%Y%m%d-%H%M`-chCHANNEL_NAME-29min.ts \
#     --timeout=1740
#
# Augment both of the duration times/timeouts (eg. 29min), channel names, &
# folder locations according to your system environment.
# NOTE: I have no idea if this will break on signal disruptions, or long files.
# ffmpeg -i /dev/dvb/adapter1/dvr0 /tmp/output.ts can also be used, but again
# with the above note, no idea how the command will react on weak signals or big
# files!  FFMPEG delays while recording, guess looking for stream header info.
#
# 2016.05.30: Replaced spaces within Channel Names with a dash.
# (ie. ${CHANNEL// /-}).  Cleaned-up some comments.
#
# 2015.04.01: Reverted to creating files using .ts suffix instead of .mpg, per
# standards.
#
# 2014.12.01: Found a bug where later versions of w_scan write VSB_8 instead of
# the usual VSB8.  Earlier versions of zap utilities cannot read this yet.  (Bug
# noted below the call for azap.)
#
# 2014.05.23: Thanks to Holger Beetz in Germany for catching missing quotes at
# line #197, around the FILE_NAME parameter for 'dd' and 'ZAP_COMMAND'.  The
# effect without quotes, users could not specify a DVB channel with spaces.
# Now, spaces within channel names (or filenames) should no longer be a problem.

#===============================================================================

# TODO: CHECK azap & cat are installed and accessible

